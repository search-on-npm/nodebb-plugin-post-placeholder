"use strict";

(function () {
  // Note how this is shown in the console on the first load of every page
  $(document).ready(function () {
    var checkPlaceholder = function (elComposer) {
      //var msg = elComposer.find('textarea').val().replace(/(^|\W)@(\w+)/g, "");
      //var msg = msg.replace(/\s/g, "");

      var msg = elComposer.find("textarea").val();

      if (msg == "") {
        elComposer.find(".composer-submit").attr("disabled", true);

        $(".msgComposer").fadeIn();
      } else {
        $(".msgComposer").hide();
        elComposer.find(".composer-submit").attr("disabled", false);
      }
    };

    $(window).on("action:composer.loaded", function (e, data) {
      var elComposer = $(".composer[data-uuid=" + data.post_uuid + "]");
      socket.emit(
        "plugins.ConnectPostPlaceholder.get",
        {},
        function (err, values) {
          if (!err) {
            if (values && values.message && values.message !== "") {
              var message = values.message.replace(/(?:\r\n|\r|\n)/g, "<br>");
              elComposer
                .find("textarea")
                .parent()
                .prepend('<div class="msgComposer">' + message + "</div>");
              checkPlaceholder(elComposer);
              elComposer
                .find("textarea")
                .on(
                  "keydown keypress keyup focus blur change",
                  function (event) {
                    checkPlaceholder(elComposer);
                  }
                );

              $(".msgComposer").on("click", function (event) {
                elComposer.find("textarea").focus();
                $(".msgComposer").hide();
              });
            }
          }
        }
      );
    });
  });
})();
