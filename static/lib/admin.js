define("admin/plugins/post-placeholder", ["settings"], function (Settings) {
  "use strict";
  /* globals $, app, socket, require */

  var ACP = {};

  ACP.init = function () {
    Settings.load(
      "connect-post-placeholder",
      $(".connect-post-placeholder-settings")
    );

    document.querySelector("#save").addEventListener("click", function () {
      let messaggio = document.getElementById("message").value;
      if (messaggio != null && messaggio != undefined && messaggio != "") {
        Settings.save(
          "connect-post-placeholder",
          $(".connect-post-placeholder-settings"),
          function () {
            app.alert({
              type: "success",
              alert_id: "connect-post-placeholder-saved",
              title: "Settings Saved",
            });
          }
        );
      } else {
        document.getElementById("message").style.border = "red";
        app.alert({
          type: "danger",
          alert_id: "connect-post-placeholder-saved",
          title: "Error",
          message: "Input Error",
        });
      }
    });
  };

  return ACP;
});
