<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<div class="panel-heading">Post Placeholder (GT)</div>
			<div class="panel-body">
				<form role="form" class="connect-post-placeholder-settings">
					<p>
					Questo plugin permette di inserire un testo che sarà visto da tutti gli utenti che vogliono pubblicare un post. Si può inserire anche la sintassi HTML
					</p>
					<div class="form-group">
						<label for="message">Message</label>
						<textarea id="message" name="message" title="Setting 1" class="form-control" placeholder="Message"></textarea>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">Control Panel</div>
			<div class="panel-body">
				<button class="btn btn-primary" id="save">Save Settings</button>
			</div>
		</div>
	</div>
</div>
