"use strict";

var controllers = require("./lib/controllers"),
  meta = require.main.require("./src/meta"),
  SocketPlugins = require.main.require("./src/socket.io/plugins"),
  plugin = {};

SocketPlugins.ConnectPostPlaceholder = {};

plugin.init = function (params, callback) {
  var router = params.router,
    hostMiddleware = params.middleware,
    hostControllers = params.controllers;
  router.get(
    "/admin/plugins/connect-post-placeholder",
    hostMiddleware.admin.buildHeader,
    controllers.renderAdminPage
  );
  router.get(
    "/api/admin/plugins/connect-post-placeholder",
    controllers.renderAdminPage
  );
  callback();
};

plugin.addAdminNavigation = function (header, callback) {
  header.plugins.push({
    route: "/plugins/connect-post-placeholder",
    icon: "fa-file-image-o",
    name: "Post Placeholder (Connect)",
  });
  callback(null, header);
};

SocketPlugins.ConnectPostPlaceholder.get = function (socket, data, callback) {
  meta.settings.get("connect-post-placeholder", function (err, options) {
    return callback(null, { message: options.message });
  });
};

module.exports = plugin;
