# Post-Placeholder Plugin for Connect

**[FUNZIONALITA']**
<br />
Questo plugin permette,lato amministratore, di settare un testo che sarà visto da tutti gli utenti che vogliono pubblicare un post.Si può inserire anche la sintassi markdown.<br />
Un esempio della pagina del plugin lato amministratore è:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-post-placeholder/raw/demaoui/screenshot/post-placeholder-admin.png)
<br />
<br />
Lato utenti:
<br />
<br />
![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-post-placeholder/raw/demaoui/screenshot/post-placeholder-post.png)

